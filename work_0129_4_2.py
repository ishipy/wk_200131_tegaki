import pandas as pd
import numpy as np
from keras.datasets import mnist
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import Adam 
from keras.utils import np_utils
import pickle
from sklearn.metrics import accuracy_score
from functools import reduce


with open("mnist/t10k-3-0.pgm", "r",encoding="utf-8") as f:
    data=f.read()
    lines =data.split("\n")

print(lines)

del lines[0]

print(lines)

question_pic = list(reduce(lambda a, b: a + b, map(lambda x: x.split(' '), lines)))

print(len(question_pic))

next_q_pic=np.array(question_pic)

"""
#リストの2次元化
last_question_pic=[question_pic]
print(last_question_pic)
"""

# データをfloat32型に変換して正規化する
X_test = next_q_pic.reshape(1, 784).astype('float32')
X_test /= 255
print(X_test)

# ラベルデータを0-9までのカテゴリを表す配列に変換 --- (*2a)
#y_train = np_utils.to_categorical(y_train, 10)
#y_test  = np_utils.to_categorical(y_test, 10)


#pickleを呼び出す
with open("mnist.pkl","rb") as f:
    pk_mnist =pickle.load(f)
    pre = pk_mnist.predict(X_test)

print(pre)


#df = pd.read_csv('/mnist/t10k-3-0.pgm')
#df.head()


#with open('C:/Users/ishii.takayuki/Desktop/pyworks/機械学習/keras_mnist-master/keras_mnist-master/mnist/t10k-3-0.pgm', "r",encoding = 'utf-8-sig') as f:
#    print(f)

